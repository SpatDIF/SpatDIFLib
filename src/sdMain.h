/** @file
 *
 * @ingroup spatdiflib
 *
 * @brief
 *
 * @details
 *
 * @authors Chikashi Miyama, Trond Lossius
 *
 * @copyright Copyright (C) 2013 - 2014 Zurich University of the Arts, Institute for Computer Music and Sound Technology @n
 * This code is licensed under the terms of the "New BSD License" @n
 * http://creativecommons.org/licenses/BSD/
 */

#pragma once
#include "sdDescriptorSpec.h"
#include "sdDate.h"

#include "sdEntity.h"
#include "sdLoader.h"
#include "sdOSCResponder.h"
#include "sdSaver.h"
#include "sdScene.h"

